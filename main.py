from flask import Flask, render_template

app = Flask(__name__)
lista_dias = ["Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira"]
lista_períodos = ["Manhã","Tarde","Noite"]
@app.route('/')

def home():
    return render_template('index.html', dias = lista_dias, períodos = lista_períodos)

if __name__ == '__main__':
    app.run(debug=True)